const path = require('path');
const webpack = require('webpack');
const config = require('node-config-env-value');

const NODE_ENV = config.get('NODE_ENV');

module.exports = {

  entry: path.join(__dirname, 'src/main.jsx'),

  output: {
    path: path.join(__dirname, 'build/src/public'),
    publicPath: '/public',
    filename: 'bundle.js',
  },


  plugins: [
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.EnvironmentPlugin({ NODE_ENV }),
  ],

  module: {
    loaders: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: [/node_modules/, /public/],

      },
      {
        test: /\.jsx$/,
        loader: 'babel-loader',
        exclude: [/node_modules/, /public/],
      },
      {
        test: /\.css$/,
        loader: 'style-loader!css-loader!autoprefixer-loader',
      },
      {
        test: /\.(png|jpg|gif|svg|woff|woff2|eot|ttf)(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url-loader?limit=10000&name=[name]-[hash].[ext]',
      },
    ],
  },

};

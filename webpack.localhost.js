const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const config = require('node-config-env-value');

const port = config.get('PORT');
const url = config.get('SERVER_URL');


module.exports = merge(common, {

  devtool: 'source-map',

  devServer: {
    contentBase: 'build/src/public',
    historyApiFallback: true,
    port,
    proxy: {
      '/api/**': {
        target: url,
        secure: false,
        changeOrigin: true,
      },
    },
  },

});

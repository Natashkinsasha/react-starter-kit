import React from 'react';
import 'semantic-ui-css/semantic.css';
import {syncHistoryWithStore} from 'react-router-redux';
import {Router, IndexRedirect, Route, browserHistory, IndexRoute} from 'react-router';

import store from '../reducers/store';

import Home from './Home/HomeContainer.jsx';
import NotFound from './NotFound.jsx';


const history = syncHistoryWithStore(browserHistory, store);

export default () => {

    return (
        <Router history={history}>
            <Route path="/">
                <IndexRoute component={Home}/>
            </Route>
            <Route path="*" component={NotFound}/>
        </Router>
    )
};




import express from 'express';
import path from 'path';
import auth from 'http-auth';
import config from 'node-config-env-value';
import proxy from 'http-proxy-middleware';


process.on('uncaughtException', (err) => {
  console.error(err.stack);
});

const PORT = config.get('PORT');
const URL = config.get('SERVER_URL');

const app = express();

const basic = auth.basic({
  realm: 'Mercify Admin',
}, (username, password, callback) => {
  callback(username === 'admin' && password === '8g4T~@J#1@26u2');
});

app.use(auth.connect(basic));


app.get('*.js', (req, res, next) => {
  req.url = `${req.url}.gz`;
  res.set('Content-Encoding', 'gzip');
  next();
});


app.use('/api', proxy({
  target: URL,
  changeOrigin: true,
}));


app.use(express.static(path.join(__dirname, 'public')));


app.get('*', (req, res) => {
  res.sendFile(`${__dirname}/public/index.html`);
});


app.listen(PORT, (err) => {
  if (err) {
    return console.error(err.stack);
  }
  return console.log(`Server start on: ${PORT}`);
});

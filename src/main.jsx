import React from 'react';
import ReactDOM from 'react-dom';

import App from './modules/App.jsx';


const Main = () => (
  <App />
);

ReactDOM.render(<Main />, document.getElementById('mount-point'));
